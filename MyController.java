import java.util.Scanner;

public class MyController {

    public static void main(String[] args) {
        double hargaJual, hargaTotal, hargaBeli, pajak, komisi, jarak, biayaPengiriman;

        System.out.println("TOKO KAMI - RUKO NEWTON BARAT");
        try (Scanner keyboard = new Scanner(System.in)) {
            // input harga beli
            System.out.print("Masukkan harga beli : ");
            hargaBeli = keyboard.nextDouble();
            // input pajak
            System.out.print("Masukkan pajak (%) : ");
            pajak = keyboard.nextDouble();
            // input komisi
            System.out.print("Masukkan komisi : ");
            komisi = keyboard.nextDouble();
            // input jarak
            System.out.print("Masukkan jarak (km) : ");
            jarak = keyboard.nextDouble();

            // menghitung biaya pengiriman
            biayaPengiriman = hitungBiayaPengiriman(jarak);

            if (jarak <= 50) {
                // menghitung harga jual
                hargaJual = hitungHargaJual(hargaBeli, pajak, komisi);
                // menghitung harga total
                hargaTotal = hitungTotal(hargaJual, biayaPengiriman);
                System.out.println("\nRincian Belanja");
                System.out.println("Harga barang : Rp." + hargaJual);
                System.out.println("Biaya pengiriman : Rp." + biayaPengiriman);
                System.out.println("Total harga yang perlu dibayarkan adalah : Rp." + hargaTotal);
            }

        } catch (Exception e) {
            System.out.println("Maaf, Tolong inputkan angka");
        }
    }

    static double hitungHargaJual(double hargaBeli, double pajak, double komisi) {
        double hargaJual = hargaBeli + (hargaBeli * pajak) / 100 + komisi;
        return hargaJual;
    }

    static double hitungBiayaPengiriman(double jarak) {
        if (jarak >= 0 && jarak <= 5) {
            return 0;
        } else if (jarak >= 5 && jarak <= 10) {
            return 10000;
        } else if (jarak >= 11 && jarak <= 50) {
            return 25000;
        } else {
            System.out.println("!!Barang tidak dapat dikirimkan, jarak terlalu jauh!!");
            return 0;
        }
    }

    static double hitungTotal(double hargaJual, double biayaPengiriman) {
        return hargaJual + biayaPengiriman;
    }
}